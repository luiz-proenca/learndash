<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_learndish');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'SPRi+-%mvOB^ERc)wC[1U,ov)sva}W6s5+Dp]:/JAevBwy@gO@!Smv6mmOKYG)M ');
define('SECURE_AUTH_KEY', 'sa<A%x?`6Nh%#-^#IC/H5G&ft;mU42%_9R2F?i.?D+N|#QK}dE!fZ-b}$BxkZ@bJ');
define('LOGGED_IN_KEY', 'P/N/nX.o6P7iqw<m`ob2|..fv_M5)8ByL_L-8Z2(AKNI=aFYx0Tg9|=?ft#g;)H]');
define('NONCE_KEY', '1x7[Kz!$_9g7u9zU2RksNlEQNa/UAzZG%+ (/wD,@l;!T&- 1XpY7M/4_t!f~E}(');
define('AUTH_SALT', 'B~^8C[fXaIyxw$xX(vv!E~K%/mIG;ApWEU:!AA$4BjSQd?fkPq3VIMhCAND5+<$ ');
define('SECURE_AUTH_SALT', 'Q49k:*C_ix%3<0x7BTwb3+/^<`BFF.2Xdh8(`$*O0P4IS0S 0LhD~b9N=@wPR]k4');
define('LOGGED_IN_SALT', 'B&}3-4WV^-C)Dlmn-7RQ{UDZP4o|m8x(lr!ehV*^;AXrI0%Y5BnfpK$)P}kNahRx');
define('NONCE_SALT', 'B_K3}Fsab[ u.=uCMmnf|J6-nD3!O4SI%j+<9?5B?KE]3gKh_^x;brX&h2+QI0@q');
define('FS_METHOD', 'direct');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__).'/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH.'wp-settings.php';
